README for a modular multithreaded News Application that searches the provided plugins websites for headlines,
extracts these headlines and displays them. The application searches each website automatically for new headlines
in a specified time frame and updates the interface simultaneously. Has functionality for manual scheduling and cancelling.

Repository File Structure:
	core:
	- Contains the primary src files which defines the interface and supporting structures.
	plugins:
	- Each folder contains its own set of code to extract headlines from a specified website.
	build files:
	- Files specifying the build engineering for the project.

This application was created using:
	- Java 1.8.0.111
	- Gradle 4.1
	
Building the Application:
	- Navigate to the main directory
	
		$ gradle build
		
Running the Application with Plugins:
	- Before attempting to run the program understand that it accepts plugins via the command line, therefore it may be convenient
	for you to put your plugins class files in the main directory for ease of access
	- Navigate to the main directory
	
		$java -jar core/build/libs/core.jar example.class example2.class example3.class

Extending the Application:
	- The application has been made modular for ease of extension, follow the format of the previous plugins.
	The only code you are required to generate yourself is the code the parse the HTML, find and extract headlines.
	
Contact Details:
	- Email: aantlegall@gmail.com
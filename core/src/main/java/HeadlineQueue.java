import java.util.concurrent.LinkedBlockingQueue;
import java.util.*;

/**
* HeadlineQueue.java - Provides a class to handle the blocking queue containing
*			the headlines. Plugins add headlines to the queue and this class takes,
*			 sorts them then adds them to the window
* Author: Antony LeGall
* Std ID: 17728357
*/
public class HeadlineQueue
{
    private LinkedBlockingQueue<String> queue;
	private AppWindow window;

	// Constructor to initialize fields
	public HeadlineQueue(AppWindow window)
	{
		this.window = window;
		queue = new LinkedBlockingQueue<String>();
	}

	// Checks to see if each new headline is already in the list of current
		// headlines and adds it if it isnt
    public void sortHeadlines()
    {
    	// Initiate a timer to cause the thread to check the queue for
    		// new headlines every 50ms
    	Timer timer = new Timer();
		TimerTask task = new TimerTask() {
			@Override
			public void run()
			{
				try
	    		{
	    			int index;
					boolean found = false;
			        String newHeadline = queue.take();
			        // Get the current headlines from the window in array form
			        Object[] currentHeadlines = window.getHeadlines().toArray();
			        // Loop through each current headline
			        for(int i = 0; i < currentHeadlines.length; i++)
			        {
			        	// The date must be removed from each headline before the
			        		// comparison
			        	String headline = (String)currentHeadlines[i];
			        	// Get index of "(" as the date is after this
			        	index = newHeadline.lastIndexOf("(");
			        	// Get a substring without date
			        	String sub = newHeadline.substring(0, index);
			        	// Check if the headline contains this substring
			        	if(headline.contains(sub))
			        	{
			        		found = true;
			        	}
			        }
			        // If its not a duplicate headline then add it to the list
			        if(!found)
			        {
						window.addHeadline(newHeadline);
			        }
		        }
	    		catch(InterruptedException ie)
		        {
		            // Notify the user that a thread interuption has occured
		            window.showError("Thread in: " + ie.getClass().getName() +
		            "Has been interupted: " + ie.getMessage());
		        }
			}
		};
		// Schedule this task the recur every 50ms
		timer.schedule(task, 0, 50);
    }

    // Allows plugins to add to the queue from outside this class.
    public void enqueue(String headline)
    {
        try
        {
            queue.put(headline);
        }
        catch(InterruptedException ie)
        {
            // Notify the user that a thread interuption has occured
            window.showError("Thread in: " + ie.getClass().getName() +
            "Has been interupted: " + ie.getMessage());
        }
    }

    // Called when the cancel button is selected on the interface, clears
    // all headlines from the queue
    public void cancel()
    {
    	queue.clear();
    }
}
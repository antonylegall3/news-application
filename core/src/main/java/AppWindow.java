import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.List;

/**
* AppWindow.java - Builds the GUI, provides functionality to its buttons
*       and provides ways to access the private fields.
* Author: Antony LeGall
* Std ID: 17728357
*/
public class AppWindow extends JFrame
{
    // Contains the current list of headlines being displayed
    private DefaultListModel<String> headlines;
    // Contains the list of urls currently being downloaded from
    private DefaultListModel<String> currentDownloads;
    private List<NewsPlugin> pluginList;
    private HeadlineQueue queue;
    private UpdateHeadlines updater;

    // AppWindows constructor performs the necessary setup
    public AppWindow(Clock clock)
    {
        // Name the application "News Feed"
        super("News Feed");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Create the tools panel which sits at the top of the GUI
            // and provides two buttons
        JPanel toolsPanel = new JPanel(new GridLayout());
        JButton updateButton = new JButton("Update");
        JButton cancelButton = new JButton("Cancel");
        JLabel headlineText = new JLabel("Current Headlines");
        JLabel downloadText = new JLabel("Current Downloads");
        headlineText.setHorizontalAlignment(JLabel.LEFT);
        downloadText.setHorizontalAlignment(JLabel.RIGHT);
        toolsPanel.add(headlineText);
        toolsPanel.add(updateButton);
        toolsPanel.add(cancelButton);
        toolsPanel.add(downloadText);
        
        // Instantiate the two containers and provide a JList to display them 
            // on the GUI
        headlines = new DefaultListModel<>();        
        JScrollPane resultsList = new JScrollPane(new JList<String>(headlines));
        currentDownloads = new DefaultListModel<>();        
        JScrollPane downloadList = new JScrollPane(new JList<String>(currentDownloads));

        // Specify where the clock will be displayed
        clock.setHorizontalAlignment(JLabel.RIGHT);

        // Start the thread to update the Clock
        Thread clockThread = new Thread(clock);
        clockThread.start();

        updater = new UpdateHeadlines(this);

        //Provide a response for when the "Update" button is pressed
        updateButton.addActionListener(new ActionListener()
        {
            @Override public void actionPerformed(ActionEvent e)
            {
                // Dispatch a new thread to handle the downloads to keep
                    // the GUI responsive
                Thread updateThread = new Thread(updater);
                updateThread.start();
            }
        });

        //Provide a response for when the "Cancel" button is pressed
        cancelButton.addActionListener(new ActionListener()
        {   
            @Override public void actionPerformed(ActionEvent e)
            {
                // Clear the current queue of headlines
                queue.cancel();
                // Stop each plugin from sending more headlines
                for(NewsPlugin plugin : pluginList)
                {
                    plugin.cancelDownload();
                }
            }
        });
        
        // Builds the main GUI and specifies where each item will be located
            // on the panel
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(toolsPanel, BorderLayout.NORTH);
        contentPane.add(resultsList, BorderLayout.WEST);
        contentPane.add(downloadList, BorderLayout.EAST);   
        contentPane.add(clock, BorderLayout.SOUTH);
        pack();
    }
    
    // Various getters, setters and functions to access the classes private fields
    public DefaultListModel getHeadlines()
    {
        return headlines;
    }

    public void addHeadline(String headline)
    {
        headlines.add(0, headline);
    }

    // Adds a url to the current download list
    public void addDownload(String download)
    {
        currentDownloads.addElement(download);
    }

    public void removeDownload(String download)
    {
        currentDownloads.removeElement(download);
    }

    public DefaultListModel getDownloads()
    {
        return currentDownloads;
    }

    public void setPlugins(List<NewsPlugin> pluginList)
    {
        this.pluginList = pluginList;
        // Sets the updaters pluginsList from this setter as it cant be done
        // anywhere else
        updater.setPlugins(pluginList);
    }

    public void setQueue(HeadlineQueue queue)
    {
        this.queue = queue;
    }

    public void showError(String message)
    {
        JOptionPane.showMessageDialog(this, message, "Error", JOptionPane.ERROR_MESSAGE);
    }
}
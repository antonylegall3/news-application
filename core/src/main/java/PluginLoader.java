import java.nio.file.*;

/**
* PluginLoader.java - Load a plugin based on the provided class name passed in.
* Author: Antony LeGall
* Std ID: 17728357
*/
public class PluginLoader extends ClassLoader
{
	public NewsPlugin loadPlugin(String fname) throws ClassNotFoundException
	{
		try
		{
			// Create a class object based on the filename
			byte[] classData = Files.readAllBytes(Paths.get(fname));
			Class<?> cls = defineClass(null, classData, 0, classData.length);
            // Return a new instance casted to a newsplugin
			return (NewsPlugin)cls.newInstance();
		}
		catch(Exception e)
		{
			throw new ClassNotFoundException(
			String.format("Could not load '%s': %s", fname, e.getMessage()), e);
		}
	}
}
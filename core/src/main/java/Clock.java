import javax.swing.*;
import java.awt.event.*;
import java.util.Date;
import java.text.DateFormat;

/**
* Clock.java - Displays the current time and date on the GUI and keeps it updated
* Author: Antony LeGall
* Std ID: 17728357
*/
public class Clock extends JLabel implements Runnable
{
	public void run()
	{
		// Create a timer to check the time every second and update the clock
        Timer timer = new Timer(1000, new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent e) {
		        setTime();
		    }
        });
        // Make the clock appear the moment the GUI is opened
        timer.setInitialDelay(0);
		timer.start();
	}

	public void setTime()
	{
		// Update the time shown on the clock to the minute
		setText(DateFormat.getDateTimeInstance(DateFormat.LONG, 
			DateFormat.SHORT).format(new Date()));
	}
}
import java.util.*;

/**
* NewsPlugin.java - Provides an interface for concrete plugin classes to implement.
* Author: Antony LeGall
* Std ID: 17728357
*/
public interface NewsPlugin extends Runnable
{
	// Various classes common among all plugins that must be implemented
	public void run();
	public void extractHeadlines(List<String> headings);
	public void setQueue(HeadlineQueue queue);
	public void setWindow(AppWindow window);
	public void cancelDownload();
	public String getURL();
	public int getUpdateFrequency();
}
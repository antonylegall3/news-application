import javax.swing.SwingUtilities;
import java.util.*;
import javax.swing.Timer;
import java.awt.event.*;

/**
* Main.java - Core function that dispatches threads, initializes
*         important objects and passes them to the necessary functions.
* Author: Antony LeGall
* Std ID: 17728357
*/
public class Main
{
    public static void main(String[] args)
    {
        PluginLoader pLoader = new PluginLoader();
        final List<NewsPlugin> pluginList = new ArrayList<NewsPlugin>();
        //Load the command line plugins into a list
        try
        {
            for(String s : args)
            {
                NewsPlugin plugin = pLoader.loadPlugin(s);
                if(plugin != null)
                {
                    pluginList.add(plugin);
                }
            }
        }
        catch(ClassNotFoundException e)
        {
            System.out.println("Error with command line arguments: " + e);
            // Exit runtime as one of the command line plugins wasnt found
            System.exit(1);
        }
        // Causes the below run method to be executed on the AWT event thread
        SwingUtilities.invokeLater(new Runnable()
        {
            @Override public void run()
            {
                // Instantiate objects necessary for the program to function
                Clock clock = new Clock();
                AppWindow window = new AppWindow(clock);
                HeadlineQueue queue = new HeadlineQueue(window);
                for(final NewsPlugin p : pluginList)
                {
                    // Set the plugins various fields
                    p.setQueue(queue);
                    p.setWindow(window);
                    // Dispatch a thread to each plugin to perform the intial download
                    //  while also starting a timer for future updates
                    Timer timer = new Timer(p.getUpdateFrequency(), new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            Thread t = new Thread(p);
                            t.start();
                        }
                    });
                    //Start the timer immediately
                    timer.setInitialDelay(0);
                    timer.start();
                }

                // Give the list of plugins to the GUI object
                window.setPlugins(pluginList);
                window.setQueue(queue);

                // Dispatch the thread to watch the headline queue
                queue.sortHeadlines();

                // Open the GUI
                window.setVisible(true);
            }
        });
    }
}
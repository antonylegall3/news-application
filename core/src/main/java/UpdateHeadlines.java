import java.util.*;

/**
* UpdateHeadlines.java - Provides a class to begin a fresh headline download 
*		for each plugin provided they arent currently downloading. This
*		is done externally from the window class as it needs its own thread to
*		execute, otherwise it freezes the GUI.
* Author: Antony LeGall
* Std ID: 17728357
*/
public class UpdateHeadlines implements Runnable
{
	private List<NewsPlugin> pluginList;
	private AppWindow window;

	public UpdateHeadlines(AppWindow window)
	{
		this.window = window;
	}

	public void run()
	{
		// Loop through each plugin checking if they are currently downloading
		for(NewsPlugin plugin : pluginList)
        {
        	Boolean found = false;
        	String url = plugin.getURL();
        	Object[] downloads = window.getDownloads().toArray();
        	for(int i = 0; i < downloads.length; i++)
        	{
        		String download = (String)downloads[i];
                // Check if the current url is already in the list of current downloads
        		if(download.equals(url))
        		{
        			window.showError(url + " is currently being updated");
        			found = true;
        		}
        	}
        	if(!found)
        	{
        		// Dispatch a thread for the plugin to update the list
    			Thread updateThread = new Thread(plugin);
				updateThread.start();
        	}
        }		
	}

	public void setPlugins(List<NewsPlugin> plugins)
	{
		pluginList = plugins;
	}
}
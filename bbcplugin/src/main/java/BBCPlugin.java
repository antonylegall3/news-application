import java.nio.ByteBuffer;
import java.nio.channels.*;
import java.io.IOException;
import java.net.URL;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.util.*;

/*
* BBCPlugin.java - A concrete implementation of the Newsplugin interface,
* 				specifically downloads from the BBC website, parses the
*				HTML, removes the headlines and adds them to the queue.
* Author: Antony LeGall
* Std ID: 17728357
*/
public class BBCPlugin implements NewsPlugin
{
	// Specify the url from which to download the headlines from, this doesnt change
	private final String url = "https://bbc.co.uk";
	// Specify in ms the frequency by which the headlines should be downloaded
		// Currently 10 minutes
	private final int updateFrequency = 600000;
	private HeadlineQueue queue;
	private AppWindow window;
	private boolean cancel = false;

	// Extract the raw heading HTML data from the specified URL
	public void run()
	{
		try
		{
			// Add this url to the GUI's download list
			window.addDownload(url);
			// Load the website into a URL object
			URL urlObj = new URL(url);
			// "try-with-resources" statement; will call chan.close() when finished
			try(ReadableByteChannel chan = Channels.newChannel(urlObj.openStream()))
			{
				// Allocate memory for the buffer
				ByteBuffer buf = ByteBuffer.allocate(65536);
				// Get the array backed by buffer
				byte[] array = buf.array();
				// Read a chunk of data
				int bytesRead = chan.read(buf);
				int index1, index2;
				String line;
				List<String> headings = new ArrayList<String>();
				Boolean done = false;

				// Continue reading until all bytes have been read
				while(bytesRead != -1)
				{
					// Read a line of HTML from the byte array from 0 to bytesRead
					line = new String(Arrays.copyOfRange(array, 0, 
						bytesRead - 1),"UTF-8");
					// Check if the line contains a heading
					if(line.contains("h3"))
					{
						index1 = 0;
						// If it does then get all the story headings from the line
						while(!done)
						{
							// Get the index of the first h3
							index1 = line.indexOf("h3", index1);
							// Get the index of the next h3
							index2 = line.indexOf("h3", index1 + 1);
							// Get everything inbetween the h3 tags
							if((index1 > 0) && (index2 > 0))
							{
								headings.add(line.substring(index1, index2));
								index1++;
							}
							else
							{
								done = true;
							}
						}
					}
					buf.clear();
					bytesRead = chan.read(buf);
					done = false;
				}
				extractHeadlines(headings);
			}
			catch(IOException e)
			{
				// Notify the user that an error has occured reading the html
           		window.showError("An error has occured downloading the headlines from: "
           		 + url + ":" + e.getMessage());
            	window.showError("Retrying");
            	// Atttempt to read the headlines again ===============!!!###
            	//getHeadings();
			}
		}
        catch(MalformedURLException me)
        {
        	// Notify the user each time the thread tries to download that there
        	// is an error with the URL
            window.showError("An error has occured with the URL: " + url + 
            	":" + me.getMessage());
        }
	}

	// Extract, clean and mark the headlines from the messy html headings
	public void extractHeadlines(List<String> headings)
	{
		CharSequence target = "&#x27;", replacement = "";
		int index1, index2;
		// Each headline lies between the second '>' and the first '<'
		for(String heading : headings)
		{
			// If the cancel button has been selected on the GUI
			//	 don't send the updated headlines
			if(cancel == false)
			{
				index1 = 0;
				//Find the first occurance of '>'
				index1 = heading.indexOf(">", index1);
				// Find the second occurance of '>'
				index1 = heading.indexOf(">", index1 + 1);
				// Find the first occurance of '<' after the second '>'
				index2 = heading.indexOf("<", index1);
				if(index1 != -1)
				{
					// Extract the the headline
					heading = heading.substring(index1 + 1, index2);
					// Skip empty headlines
					if(!heading.isEmpty())
					{
						// Remove the random characters from the headlines
						heading = heading.replace(target, replacement);
						// Stamp the headline with the URL and current time/date
						// And add it to the blocking queue
						queue.enqueue(url + ": " + heading + " (" +
							(DateFormat.getDateTimeInstance(DateFormat.SHORT,
								 DateFormat.SHORT).format(new Date())) + ")");
					}
				}
			}
		}
		// Remove this url from the GUI's download list
		window.removeDownload(url);
		// Set this back once the headlines havent been sent
		cancel = false;
	}

	// Setters to intialize the private fields
	public void setQueue(HeadlineQueue queue)
	{
		this.queue = queue;
	}

	public void setWindow(AppWindow window)
	{
		this.window = window;
	}

	// Function called when cancel is selected on the GUI, sets the cancel 
	//	flag to true to prevent the updated headlines from sending
	public void cancelDownload()
	{
		cancel = true;
		window.removeDownload(url);
	}

	// Getters to access the private fields from outside the class
	public String getURL()
	{
		return url;
	}

	public int getUpdateFrequency()
	{
		return updateFrequency;
	}
}